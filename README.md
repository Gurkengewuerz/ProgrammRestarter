Auto Restarter
--------------
The auto restarter restarts a process list automaticly after x minutes.

Example Config File (*ProcessRestarter.cfg*):

    "C:\Users\Administrator\Desktop\BotNetwork\bot1\NecroBot.exe"
    "C:\Users\Administrator\Desktop\BotNetwork\bot2\NecroBot.exe"
    "C:\Users\Administrator\Desktop\BotNetwork\bot3\NecroBot.exe"

The program takes two alternative argument.
[restart Time in seconds] [Sleep between process starting in milli seconds]

*defaults:*

 - restart Time in seconds: 30 seconds
 - Sleep between process starting in milli seconds: 500 milliseconds


**Windows:**
Therefor create a shortcut and add the minutes at the end like

    "Z:\C#\ProgramRestarter\ProgramRestarter\bin\Debug\ProgramRestarter.exe" 60
    "Z:\C#\ProgramRestarter\ProgramRestarter\bin\Debug\ProgramRestarter.exe" 60 250



Website: [Gurkengewuerz](https://gurkengewuerz.de/web)

Twitter: [Gurkengewuerz](https://twitter.com/Gurkengewuerz)