﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace ProcessRestarter
{
    public class Exe
    {
        public Process exeProcess { get; set; }
        public string exePath { get; set; }
    }

    class Program
    {

        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            switch (sig)
            {
                case CtrlType.CTRL_C_EVENT:
                case CtrlType.CTRL_LOGOFF_EVENT:
                case CtrlType.CTRL_SHUTDOWN_EVENT:
                case CtrlType.CTRL_CLOSE_EVENT:
                    stop();
                    return false;
                default:
                    return false;
            }
        }

        static List<Exe> processList = new List<Exe>();
		static string exePath;
        static int waitTime = 500;
		static string[] Filecache;
        static void Main(string[] args)
        {
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);

            int argsTime = 30;
            if(args.Length >= 1)
            {
                try
                {
                    argsTime = Convert.ToInt32(args[0]);
                    Logger.log("Using Argument Time: " + argsTime);
                    if(args.Length >= 2)
                    {
                        waitTime = Convert.ToInt32(args[1]);
                        Logger.log("Using custom waittime (in milli seconds): " + waitTime);
                    }
                }
                catch (Exception)
                {
                    Logger.error("arguments are broken!");
                    Console.ReadLine();
                    Environment.Exit(0);
                    throw;
                }
            }

            Logger.debug("Try to load \"ProcessRestarter.cfg\"...");
            exePath = AppDomain.CurrentDomain.BaseDirectory;
			if (!File.Exists(Path.Combine(exePath, "ProcessRestarter.cfg")))
			{
				Logger.error("\"ProcessRestarter.cfg\" does not existist.");
				Logger.error("Path of exe: " + exePath);
				Logger.error("Generate one with the following rules: ");
				Logger.error("    for each row one process (exe) as Stromg (starting and end with\")");
				for (int i = 0; i < 7; i++)
				{
					Console.WriteLine("");
				}
				Logger.log("Press any key to exit...");
				Console.ReadLine();
				return;
			}

			start();

            if(processList.Count <= 0)
            {
                Logger.error("Proceslist is empty. Shutting down!");
                for (int i = 0; i < 7; i++)
                {
                    Console.WriteLine("");
                }
                Logger.log("Press any key to exit...");
                Console.ReadLine();
                return;
            }

            while (true)
            {
                DateTime now = DateTime.Now;
                DateTime in30min = now.AddMinutes(argsTime);
                Logger.info("Timer started at: " + now.ToShortTimeString());
                Logger.info("Next restart: " + in30min.ToShortTimeString());
                while (DateTime.Now < in30min)
                {
                    System.Threading.Thread.Sleep(60000); // Sleep 60 Sekunden
                }

                Logger.info(argsTime +" minutes passed, restarting...");
                restart();
            }
        }

        private static void stop()
        {
            Console.WriteLine("STOP");
            foreach (Exe exe in processList)
            {
                try
                {
                    if (IsRunning(exe.exeProcess))
                    {
                        Logger.debug("stop " + exe.exeProcess.ProcessName);
                        exe.exeProcess.Kill();
                    }
                    else
                    {
                        Logger.error("FAILED TO STOP! " + exe.exePath);
                    }
                }
                catch (Exception e)
                {
                    Logger.error("Failed to stop " + exe.exeProcess.ProcessName);
                    Logger.printStackTrace(e.StackTrace);
                }
            }
        }

        private static void start()
        {
            Console.WriteLine("START");
			processList.Clear();
			string[] exeToMonitor = Filecache;
			try
			{
				exeToMonitor = File.ReadAllLines(Path.Combine(exePath, "ProcessRestarter.cfg"));
				Filecache = exeToMonitor;
				Logger.log("Reload Config!");
			}
			catch (Exception ex)
			{
				Logger.error("Cant reload the Config, using the latest!");
				Logger.printStackTrace(ex.StackTrace);
			}
			foreach (string str in exeToMonitor)
			{
				try
				{
					Exe exe = new Exe();
					ProcessStartInfo startInfo = new ProcessStartInfo();
					startInfo.WorkingDirectory = Path.GetDirectoryName(str.Replace("\"", ""));
					startInfo.FileName = str;
					startInfo.WindowStyle = ProcessWindowStyle.Normal;
					Process process = Process.Start(startInfo);
					exe.exeProcess = process;
					exe.exePath = str;
					processList.Add(exe);
					Logger.log(str + " added to process list and is started...");
					System.Threading.Thread.Sleep(waitTime);
				}
				catch (Exception e)
				{
					Logger.error("Failed to Start: " + str);
					Logger.printStackTrace(e.StackTrace);
				}
			}
        }

        private static void restart()
        {
            stop();
            start();
        }

        public static bool IsRunning(Process process)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            try
            {
                Process.GetProcessById(process.Id);
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }
    }
}
